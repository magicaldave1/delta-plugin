/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::plugin::Plugin;
use crate::record::cell_ref::CellRefId;
use crate::record::{DeltaRecordType, MetaRecord, RecordId, RecordType};
use crate::serialize::load_plugin;
use anyhow::{Context, Result};
use hashlink::LinkedHashMap;
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PluginPatch {
    pub source_file: PathBuf,
    pub dest_file: PathBuf,
    #[serde(flatten)]
    pub records: LinkedHashMap<RecordId, MetaRecord>,
}

fn apply_patch(plugin: Plugin, patch: PluginPatch) -> Plugin {
    let mut patch_plugin = Plugin::blank(patch.source_file.clone());
    patch_plugin.records = patch.records;

    let mut merged = Plugin::blank(plugin.file.clone());
    merged.header = plugin.header.clone();

    for (id, record) in plugin.records {
        if let Some(patch_record) = patch_plugin.records.remove(&id) {
            if let MetaRecord::Record(record) = record {
                match patch_record {
                    MetaRecord::Delta(_) => {
                        let mut new = record;
                        new.get_full_record_mut().apply_patch(patch_record.clone());
                        merged.records.insert(id.clone(), MetaRecord::Record(new));
                    }
                    MetaRecord::Delete => {
                        // Skip inserting record since the patch deletes it
                    }
                    MetaRecord::Record(_) => {
                        merged.records.insert(id.clone(), patch_record);
                    }
                    MetaRecord::Unknown => (),
                }
            }
        } else {
            merged.records.insert(id.clone(), record);
        }
    }
    for (id, record) in patch_plugin.records {
        // Add any new records which didn't previously exist, and warn if there are any deltas remaining
        match record {
            MetaRecord::Record(_) => {
                merged.records.insert(id, record);
            }
            MetaRecord::Delta(_) => warn!(
                "Skipping applying delta in patch which has no matching record in the original plugin"
            ),
            MetaRecord::Delete => warn!(
                "Skipping applying deleted record in patch which has no matching record in the original plugin"
            ),
            MetaRecord::Unknown => unreachable!(),
        }
    }
    merged
}

// create_delta uses create_master_record_map, which will treat cells and dialogue somewhat
// differently due to how they are handled in the engine. For patches, we want to ignore this
// special vanilla override behaviour since we work directly on the files
pub fn create_delta_no_vanilla(new: Plugin, original: Plugin) -> Plugin {
    // FIXME: This isn't handling cell attributes properly for some reason.
    // E.g. see bcsounds-patch.yaml, which produces an empty DeltaCell
    let mut new_plugin = Plugin::blank(new.file.clone());
    new_plugin.header = new.header.clone();

    for (id, record) in new.records {
        match record {
            MetaRecord::Record(inner) => {
                // Find record matching identifier in masters
                // If records are different, create delta between master record and this record.
                let fullrecord = inner.get_full_record();
                match original.records.get(&id) {
                    Some(MetaRecord::Delete) => {
                        new_plugin.records.insert(id, MetaRecord::Record(inner));
                    }
                    Some(MetaRecord::Record(master)) => {
                        if let Some(patch) = fullrecord.get_patch(master, false) {
                            new_plugin.records.insert(id, patch);
                        }
                    }
                    Some(MetaRecord::Delta(_) | MetaRecord::Unknown) => unreachable!(),
                    None => {
                        new_plugin.records.insert(id, MetaRecord::Record(inner));
                    }
                }
            }
            MetaRecord::Delta(_) | MetaRecord::Delete => {
                new_plugin.records.insert(id.clone(), record);
            }
            MetaRecord::Unknown => (),
        }
    }
    new_plugin
}

fn diff_plugins(original: Plugin, new: Plugin) -> PluginPatch {
    let mut patch = PluginPatch {
        records: LinkedHashMap::new(),
        source_file: original.file.clone(),
        dest_file: new.file.clone(),
    };

    // Detect deleted records, since create_delta is designed to compare against
    // masters in the style of loading and won't do so
    let mut deleted = vec![];
    let mut deleted_cellrefs: LinkedHashMap<RecordId, Vec<CellRefId>> = LinkedHashMap::new();
    for (id, record) in &original.records {
        if !new.records.contains_key(id) {
            deleted.push(id.clone());
        }
        if let MetaRecord::Record(RecordType::Cell(cell)) = record {
            if let Some(MetaRecord::Record(RecordType::Cell(new_cell))) = new.records.get(&id) {
                let mut cell_deleted_cellrefs = vec![];
                for reference in cell.references.keys() {
                    if !new_cell.references.contains_key(reference) {
                        cell_deleted_cellrefs.push(reference.clone());
                    }
                }
                if !cell_deleted_cellrefs.is_empty() {
                    deleted_cellrefs.insert(id.clone(), cell_deleted_cellrefs);
                }
            }
        }
    }

    let new = create_delta_no_vanilla(new, original);

    patch.records = new.records;

    for id in deleted {
        patch.records.insert(id, MetaRecord::Delete);
    }
    for (id, cellrefs) in deleted_cellrefs {
        use crate::delta::DeltaUnion;
        if let Some(MetaRecord::Delta(DeltaRecordType::DeltaCell(cell))) =
            patch.records.get_mut(&id)
        {
            for cellref in cellrefs {
                if let Some(references) = &mut cell.references {
                    references.insert(cellref, DeltaUnion::Delete);
                } else {
                    cell.references = Some(LinkedHashMap::new());
                    cell.references
                        .as_mut()
                        .unwrap()
                        .insert(cellref, DeltaUnion::Delete);
                };
            }
        } else {
            let mut delta_cell = crate::record::cell::DeltaCell {
                references: Some(LinkedHashMap::new()),
                ..Default::default()
            };
            if let Some(references) = &mut delta_cell.references {
                for cellref in cellrefs {
                    references.insert(cellref, DeltaUnion::Delete);
                }
            }
            patch.records.insert(
                id,
                MetaRecord::Delta(DeltaRecordType::DeltaCell(delta_cell)),
            );
        }
    }

    patch
}

/// Diffs two files and produces a patch in yaml format
pub fn diff(original: &Path, modified: &Path) -> Result<String> {
    let mut original = load_plugin(original)?;
    let mut new = load_plugin(modified)?;
    // Clear header fields which aren't helpful to the diff. If they end up being included
    // it may prevent the patch from being applied
    original.header.transpiler_version = None;
    original.header.clear_private();
    new.header.transpiler_version = None;
    new.header.clear_private();

    Ok(serde_yaml::to_string(&diff_plugins(original, new))?)
}

/// Applies a patch file to the paths included inside the file
pub fn apply(patch_path: &Path) -> Result<()> {
    let mut patch_file = File::open(patch_path)?;
    let mut patch_string = String::new();
    patch_file.read_to_string(&mut patch_string)?;
    let patch: PluginPatch = serde_yaml::from_str(&patch_string)?;
    let mut original = load_plugin(&patch.source_file)?;

    original.header.transpiler_version = None;
    original.header.clear_private();

    info!(
        "Applying patch {} to {}",
        patch_path.display(),
        original.file.display()
    );

    let new_file_name = patch.dest_file.clone();

    let mut patched = apply_patch(original, patch);

    patched.file = new_file_name.clone();
    let bin: esplugin::Plugin = patched.try_into().context(format!(
        "Failed to serialize plugin {} as esm",
        new_file_name.display()
    ))?;
    bin.write_file()?;
    Ok(())
}
