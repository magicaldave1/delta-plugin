/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::types::{AttributeType, EffectType, RangeType, SkillType};

use crate::util::RecordError;
use esplugin::Subrecord;
use nom::number::complete::{le_i32, le_i8, le_u16};
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;

#[skip_serializing_none]
#[derive(Copy, Clone, Eq, Hash, PartialEq, Debug, Serialize, Deserialize)]
pub struct Effect {
    effect_type: EffectType,
    /// Note: Only valid with certain effect types
    /// Ideally this should be data associated with
    /// the corresponding variants of the EffectType enum
    skill: Option<SkillType>,
    /// Note: Only valid with certain effect types
    /// Ideally this should be data associated with
    /// the corresponding variants of the EffectType enum
    attribute: Option<AttributeType>,
    range: RangeType,
    area: u64,
    duration: u64,
    min_magnitude: i32,
    max_magnitude: i32,
}

impl TryFrom<&[u8]> for Effect {
    type Error = RecordError;

    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        let (data, effect_type) = le_u16(data)?;
        let effect_type = FromPrimitive::from_u16(effect_type)
            .ok_or(RecordError::InvalidEffectId(effect_type as i32))?;

        let (data, skill) = le_i8(data)?;
        let skill = if skill == -1 {
            None
        } else {
            Some(FromPrimitive::from_i8(skill).ok_or(RecordError::InvalidSkillId(skill as i32))?)
        };

        let (data, attribute) = le_i8(data)?;
        let attribute = if attribute == -1 {
            None
        } else {
            Some(
                FromPrimitive::from_i8(attribute)
                    .ok_or(RecordError::InvalidAttributeId(attribute as i32))?,
            )
        };

        let (data, range) = le_i32(data)?;
        let range = FromPrimitive::from_i32(range).ok_or(RecordError::InvalidRangeId(range))?;

        let (data, area) = le_i32(data)?;
        let (data, duration) = le_i32(data)?;
        let (data, min_magnitude) = le_i32(data)?;
        let (_, max_magnitude) = le_i32(data)?;

        Ok(Effect {
            effect_type,
            skill,
            attribute,
            range,
            area: area as u64,
            duration: duration as u64,
            min_magnitude: min_magnitude as u64,
            max_magnitude: max_magnitude as u64,
        })
    }
}

impl Into<Subrecord> for Effect {
    fn into(self) -> Subrecord {
        let mut data = vec![];
        data.extend((self.effect_type as i16).to_le_bytes().iter());
        data.extend(
            self.skill
                .map(|x| [x as u8])
                .unwrap_or_else(|| (-1 as i8).to_le_bytes())
                .iter(),
        );
        data.extend(
            self.attribute
                .map(|x| [x as u8])
                .unwrap_or_else(|| (-1 as i8).to_le_bytes())
                .iter(),
        );
        for value in [
            self.range as u64,
            self.area,
            self.duration,
            self.min_magnitude,
            self.max_magnitude,
        ]
        .iter()
        {
            data.extend((*value as i32).to_le_bytes().iter());
        }
        Subrecord::new(*b"ENAM", data, false)
    }
}
