/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{nt_str, push_str_subrecord, subrecords_len, take_nt_str, RecordError};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashSet;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

/// A character attribute representing the birthsign corresponding to when they were born
/// Selected during character creation and has a set of associated powers
#[derive(
    Constructor, DeltaRecord, FullRecord, Default, Clone, Debug, PartialEq, Serialize, Deserialize,
)]
pub struct BirthSign {
    /// Name of the birthsign displayed in game
    name: String,
    /// Image representing the birthsign
    texture: String,
    /// Description of the birthsign
    desc: String,
    /// List of power identifiers
    powers: LinkedHashSet<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<BirthSign> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<BirthSign>, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut texture = None;
        let mut desc = None;
        let mut powers = LinkedHashSet::new();

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"TNAM" => texture = Some(take_nt_str(data)?.1),
                b"DESC" => desc = Some(take_nt_str(data)?.1),
                b"NPCS" => {
                    let (_, power) = take_nt_str(data)?;
                    powers.insert(power);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let name = name.ok_or(RecordError::MissingSubrecord("FNAM"))?;
        let desc = desc.ok_or(RecordError::MissingSubrecord("DESC"))?;
        let texture = texture.ok_or(RecordError::MissingSubrecord("TNAM"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::BirthSign, id),
            BirthSign::new(name, texture, desc, powers),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<BirthSign> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_str_subrecord(&mut subrecords, &self.record.texture, "TNAM")?;
        push_str_subrecord(&mut subrecords, &self.record.desc, "DESC")?;
        for power in self.record.powers {
            subrecords.push(Subrecord::new(*b"NPCS", nt_str(&power)?, false));
        }
        let header = RecordHeader::new(*b"BSGN", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
